"""CVWeb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Profile.views import index_page
from Profile.views import index_page_about
from Profile.views import index_page_work
from Profile.views import index_page_contact
from Profile.views import index_page_resume
from Profile.views import index_schedule
from Profile.views import index_delete_schedule


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index_page, name='base'),
    path('about', index_page_about, name='about'),  
    path('work', index_page_work, name='work'),
    path('contact', index_page_contact, name='contact'),
    path('resume', index_page_resume, name='resume'),
    path('schedule', index_schedule, name='schedule'),
    path('delete/', index_delete_schedule, name='delete'),
]
