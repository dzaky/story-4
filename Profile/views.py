from django.shortcuts import render
from .forms import JadwalKegiatanForm
from django.http import HttpResponseRedirect
from .models import jadwalKegiatan

response = {}

def index_page(request):
    return render(request, 'base.html')

def index_page_about(request):
    return render(request, 'story3_about.html')

def index_page_work(request):
    return render(request, 'story3_work.html')

def index_page_contact(request):
    return render(request, 'story3_contact.html')

def index_page_resume(request):
    return render(request, 'story3_resume.html')

def index_schedule(request):
    html = 'story3_registrasi.html'
    daftarkegiatan = jadwalKegiatan.objects.all().values()
    response['daftarkegiatan']=daftarkegiatan

    if request.method == 'POST':
        form = JadwalKegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('schedule')
        else:
            form = JadwalKegiatanForm()
        return render(request, "story3_registrasi.html", {'form' : form})

    form = JadwalKegiatanForm()
    response['form']=form
    return render(request, 'story3_registrasi.html',response)

def index_show_schedule(request):
    temp = JadwalKegiatanForm.objects.all()
    return render(request, 'show.html', {'table':temp})

def index_delete_schedule(request):
    hapus = jadwalKegiatan.objects.all().delete()
    form = JadwalKegiatanForm()
    return HttpResponseRedirect('/schedule')