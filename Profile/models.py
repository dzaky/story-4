from django.db import models
from datetime import datetime, date
from django.utils import timezone

# Create your models here.
class jadwalKegiatan(models.Model):
    time = models.TimeField(null=True)
    date = models.DateField(null=True)
    name = models.CharField(max_length = 200)
    location = models.CharField(max_length = 300)
    category = models.CharField(max_length = 300)

    def ___str__(self):
        return self.name