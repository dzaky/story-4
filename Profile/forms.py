from django import forms
from .models import jadwalKegiatan

class JadwalKegiatanForm(forms.ModelForm):
    class Meta:
        model = jadwalKegiatan
        fields = ('name','location','time','date','category')

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':'Isi nama'}),
            'location': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':'isi tempat'}),
            'date': forms.DateInput(attrs={'class': 'form-control','type':'date'}),
            'time': forms.TimeInput(attrs={'class':'form-control','type':'time'}),
            'category': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':'isi kategori'}),
        }

        labels = {
            'name': 'Nama Kegiatan',
            'location': 'Tempat Kegiatan',
            'date': 'Tanggal Kegiatan',
            'time': 'Jam Kegiatan',
            'category': 'Kategori Kegiatan',
        }

        error_messages = {
            'name': {'max_length': 'Terlalu banyak karakter yang dimasukan'},
            'location': {'max_length': 'Terlalu banyak karakter yang dimasukan'},
            'date': {'invalid': 'Invalid input format'},
            'time': {'invalid': 'Invalid input format'},
            'category': {'max_length': 'Terlalu banyak karakter yang dimasukan'},
        }